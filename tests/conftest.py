# -*- coding: utf-8 -*-
import os.path
import sys

# Expand sys.path with module source.
_ROOT_DIR = os.path.normpath(os.path.join(
    os.path.dirname(os.path.abspath(__file__)), '..')
)
sys.path.append(_ROOT_DIR)
